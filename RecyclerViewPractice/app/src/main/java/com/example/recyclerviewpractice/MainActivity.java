package com.example.recyclerviewpractice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private MahasiswaAdapter adapter;
    private ArrayList<Mahasiswa> mahasiswaArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new MahasiswaAdapter(mahasiswaArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    void addData(){
        mahasiswaArrayList = new ArrayList<>();
        mahasiswaArrayList.add(new Mahasiswa("Maulana Pandudinata", "222011379", "081234567899"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 1", "222011111", "081111111111"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 2", "222011222", "082222222222"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 3", "222011333", "083333333333"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 4", "222011444", "084444444444"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 5", "222011555", "085555555555"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 6", "222011666", "086666666666"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 7", "222011777", "087777777777"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 8", "222011888", "088888888888"));
        mahasiswaArrayList.add(new Mahasiswa("Coba 9", "222011999", "089999999999"));
    }
}