# PPK-Praktikum 12

# Identitas

```
Nama : Maulana Pandudinata
NIM  : 222011379
Kelas: 3SI3

```

# Deskripsi
RecyclerView sebagai alternatif dari ListView memudahkan untuk menampilkan kumpulan data dalam jumlah besar secara efisien. Anda menyediakan data dan menentukan tampilan setiap item, dan library RecyclerView secara dinamis membuat elemen saat diperlukan. RecyclerView mendaur ulang elemen individual tersebut. Saat item di-scroll keluar layar, RecyclerView tidak merusak tampilannya. Sebaliknya, RecyclerView menggunakan kembali tampilan tersebut untuk item baru yang telah di-scroll di layar. Penggunaan ulang ini sangat meningkatkan performa, meningkatkan daya respons aplikasi, dan mengurangi pemakaian daya. 
Selain menjadi nama class, RecyclerView juga merupakan nama library. Di halaman ini, RecyclerView di code font selalu berarti class dalam library RecyclerView.


# Kegiatan Praktikum

## Tampilan Data Mahasiswa
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum12/-/raw/main/Screenshot/Screenshot%20(1).png)
## Toast Ketika Salah Satu Mahasiswa di Klik
![cd_catalog.xml](https://gitlab.com/222011379/ppk-praktikum12/-/raw/main/Screenshot/Screenshot%20(2).png)

